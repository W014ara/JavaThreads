//импорт пакетов
package Threads;
//подключение библиотек
import java.util.ArrayList;
import java.util.Scanner;

//основной класс программы
public class Threads {
    //основной метод программы
    public static void main(String[] args) throws InterruptedException {
        //создание объекта класса Scanner
        Scanner input = new Scanner(System.in);
        int caseVar = -1;
        show_menu();
        System.out.print("Введите номер вашей команды: ");
        //главное меню программы
        do {
            caseVar = input.nextInt();
                switch (caseVar) {
                    case 1:
                        System.out.println("<---------Информация по отработанному пункту программы----------------->");
                        starting_threads();
                        break;
                    case 2:
                        show_menu();
                        break;
                    case 3:
                        starting_threads();
                        System.out.println("Программа успешно завершена");
                        break;
                    default:
                        System.out.println("Команда не найдена" + "\n");
                        show_menu();
                        break;
                }
        } while (caseVar != 3);
        input.close();
    }
    //метод, показывающий меню программы
    private static void show_menu() {
        System.out.println("1 - Запустить/повторить программу");
        System.out.println("2 - Вызвать меню заново");
        System.out.println("3 - Выйти из программы");
    }
    //метод создания и отработки потоков
    private static void starting_threads()
    {
        try {
        ArrayList<JThread> list = new ArrayList<JThread>();
        for (int i = 0; i < 5; ++i) {
            JThread thread = new JThread(Integer.toString(i));
            list.add(thread);
        }
        for (int i = 0; i < list.size(); ++i) {
            list.get(i).start();
        }
    }
    catch (Exception e){
        System.out.println("Произошла непредвиденная ошибка!");
    }
    }
}
