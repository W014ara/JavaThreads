//импорт пакетов
package Threads;
//подключение библиотек
import java.util.concurrent.ThreadLocalRandom;

public class JThread extends Thread {
    private String name;
    //Конструктор с параметром name
    JThread(String name)
    {this.name = name;}
    //метод, возвращающий название объекта класса JThread
    private String get_name(){return this.name;}
    @Override
    //основной метод класса JThread
    public void run()
    {
        //переменная, отвечающая за создание случайного интервала времени от 3 до 5 секунд
        int random_time= ThreadLocalRandom.current().nextInt(3000, 5000);
        try {
            Thread.sleep(random_time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Thread.currentThread().interrupt();
        if (Thread.currentThread().isInterrupted()){System.out.println("Поток " + get_name() + " остановлен" + "(" +
                random_time / 1000 + " с.)");}
        else {System.out.println("Поток " + get_name() + " продолжает работать");}
    }
}
